;;; isa.el -- ISA Team Macros
;;; Commentary:
;;; Code:

;; ==============================================
;; Définition des fonctions principale du package
;; ==============================================

(defun toggle-visit-file (file-name)
;;
;  **********************************************************************
;  "Non interactive parameterized function to be used
;  to perform ISA-COM-FILE* functions. C Gerald Masini,
;  modified by toF Winkler"
;;;
;**********************************************************************

  (if (get-file-buffer file-name)
    (if (file-exists-p file-name)
  ;; --------------------------
  ;; both buffer and file exist
  ;; --------------------------
      (progn
        (find-file file-name)
	t
      )
  ;; -------------------------------------
  ;; buffer exists and file does not exist
  ;; -------------------------------------
      (find-file file-name)
      (message
        "Warning: File associated with buffer %s is not created."
        (buffer-name))
      (beep)
      t
    );;if
    (if (file-exists-p file-name)
  ;; -------------------------------------
  ;; buffer does not exist and file exists
  ;; -------------------------------------
      (progn
        (find-file file-name)
	t
      )
  ;; -----------------------------
  ;; neither buffer nor file exist
  ;; -----------------------------
      nil
  ;; -----------------------------
    );;if
  );;if
)


; Pour les polices et les couleurs
;(setq options-save-faces t)

(defun toggle-source()
  "Toggle C/C++ file from header to source file or vice versa"
  (interactive)
  (let ((bfn (buffer-file-name))
        (case-fold-search nil))
    (if bfn 
        (if ( or (string-match "\\.cc$" bfn)
		 (string-match "\\.c\\+\\+$" bfn)
		 (string-match "\\.c$" bfn)
		 (string-match "\\.C$" bfn)
		 (string-match "\\.H$" bfn) )
	  (let ((new-extension ".h"))
             (if (string-match "\\.C$" bfn)
		 (setq new-extension ".H"))
	     (if (string-match "\\.H$" bfn)
		 (setq new-extension ".C"))
             (setq new-bfn (replace-match new-extension t t bfn))
	     (if (not (toggle-visit-file new-bfn))
		 (error "Could not find file: %s" new-bfn))
          )
	  (if (string-match "\\.h$" bfn)
	      (if (not (toggle-visit-file (replace-match ".cc" t t bfn)))
		  (if (not (toggle-visit-file (replace-match ".c++" t t bfn)))
		      (if (not (toggle-visit-file (replace-match ".c" t t bfn)))
			  (error "Could not find any file corresponding to %s" bfn))
		  )
	      )
	      (error "File extension is not managed"))
        )
      )
   )
)

;; ============================
;; Passage Header-Source en C++
;; ============================

(add-hook 'c++-mode-hook
  '(lambda ()
     (define-key c++-mode-map "\M-ss" 'toggle-source)))

(add-hook 'c-mode-hook
  '(lambda ()
     (define-key c-mode-map   "\M-ss" 'toggle-source)))

(provide 'isa-macros)
